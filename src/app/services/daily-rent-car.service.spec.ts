import { TestBed } from '@angular/core/testing';

import { DailyRentCarService } from './daily-rent-car.service';
import { HttpClientTestingModule } from "@angular/common/http/testing";

describe('DailyRentCarService', () => {
  let service: DailyRentCarService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(DailyRentCarService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
