import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { Car, Category } from "../models/daily-rent-car.model";

@Injectable({
  providedIn: 'root'
})
export class DailyRentCarService {

  private categoriesUrl = '/assets/mock-data/categoryList.json';
  private carsUrl = '/assets/mock-data/carsList.json';

  constructor(private http: HttpClient) {}

  getCategories(): Observable<Category[]> {
    return this.http.get<Category[]>(this.categoriesUrl);
  }

  getCars(): Observable<any> {
    return this.http.get(this.carsUrl);
  }

}
