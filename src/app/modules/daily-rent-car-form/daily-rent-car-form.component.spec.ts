import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DailyRentCarFormComponent } from './daily-rent-car-form.component';
import { HttpClientModule } from "@angular/common/http";
import { DailyRentCarService } from "../../services/daily-rent-car.service";

describe('DailyRentCarFormComponent', () => {
  let component: DailyRentCarFormComponent;
  let fixture: ComponentFixture<DailyRentCarFormComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DailyRentCarFormComponent],
    });
    fixture = TestBed.createComponent(DailyRentCarFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
