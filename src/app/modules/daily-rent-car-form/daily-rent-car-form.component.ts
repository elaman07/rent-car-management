import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from "@angular/forms";
import { Car, Category, RENT_PERIOD } from "../../models/daily-rent-car.model";
import { DailyRentCarService } from "../../services/daily-rent-car.service";

@Component({
  selector: 'app-daily-rent-car-form',
  templateUrl: './daily-rent-car-form.component.html',
  styleUrls: ['./daily-rent-car-form.component.scss']
})
export class DailyRentCarFormComponent implements OnInit {
  carRentalForm!: FormGroup;
  categories!: Category[];
  allCars!: Car[];
  carsByCategory!: Car[];
  price!: number;

  constructor(private fb: FormBuilder,
              private carRentalService: DailyRentCarService) {
  }

  ngOnInit(): void {
    this.fillForm();
    this.getCategoryList();
    this.getCars();
  }

  fillForm() {
    this.carRentalForm = this.fb.group({
      category: '',
      car: '',
      startDate: '',
      endDate: '',
    });
  }

  getCategoryList() {
    this.carRentalService.getCategories().subscribe((categories: Category[]) => {
      this.categories = categories;
    });
  }

  getCars() {
    this.carRentalService.getCars().subscribe((cars: Car[]) => {
      this.allCars = cars;
      this.carsByCategory = [...this.allCars];
    })
  }

  filterCars() {
    const selectedCategory = this.carRentalForm.value.category;
    this.carsByCategory = this.allCars.filter(car => car.categoryCode === selectedCategory);
  }

  onChangeCar() {
    this.calculateCost();
  }

  calculateCost() {
    const selectedCarId = this.carRentalForm.value.car;
    const startDate = new Date(this.carRentalForm.get('startDate')?.value);
    const endDate = new Date(this.carRentalForm.get('endDate')?.value);

    if (!isNaN(startDate.getTime()) && !isNaN(endDate.getTime())) {
      const timeDiff = endDate.getTime() - startDate.getTime();
      const daysDiff = Math.ceil(timeDiff / (1000 * 3600 * 24));
      const selectedCar = this.carsByCategory.find((car) => car.id === selectedCarId);

      if (selectedCar) {
        if (daysDiff >= RENT_PERIOD.MIN_RANGE_START_DAY && daysDiff <= RENT_PERIOD.MIN_RANGE_END_DAY) {
          this.price = selectedCar.price.singleDayPrice * daysDiff;
        }
        else if (daysDiff >= RENT_PERIOD.MID_RANGE_START_DAY && daysDiff <= RENT_PERIOD.MID_RANGE_END_DAY) {
          this.price = selectedCar.price.lessThanWeekPrice * daysDiff;
        }
        else if (daysDiff >= RENT_PERIOD.MAX_RANGE_START_DAY) {
          this.price = selectedCar.price.moreThanWeekPrice * daysDiff;
        }
      }
    }
  }


}
