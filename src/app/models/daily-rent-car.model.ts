export interface Category {
  title: string;
  code: string;
}

export interface RentPrice {
  singleDayPrice: number;
  lessThanWeekPrice: number;
  moreThanWeekPrice: number;
}

export interface Car {
  title: string;
  id: string;
  categoryCode: string;
  category: Category;
  price: RentPrice;
}

export const RENT_PERIOD = {
  MIN_RANGE_START_DAY: 1,
  MIN_RANGE_END_DAY: 1,
  MID_RANGE_START_DAY: 2,
  MID_RANGE_END_DAY: 5,
  MAX_RANGE_START_DAY: 6
}
