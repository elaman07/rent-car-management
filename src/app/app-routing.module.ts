import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DailyRentCarFormComponent } from "./modules/daily-rent-car-form/daily-rent-car-form.component";

const routes: Routes = [
  {
    path: '',
    component: DailyRentCarFormComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
