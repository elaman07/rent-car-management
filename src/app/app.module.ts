import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DailyRentCarFormModule } from "./modules/daily-rent-car-form/daily-rent-car-form.module";

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    DailyRentCarFormModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
